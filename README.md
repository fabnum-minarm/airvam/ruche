# --------------------------------------------------------------------------------- #
# author            : Vincent Lainé                                                              #
# creating date     : 13/04/2023                                                              #
# Last modification : 13/04/2023                                                              #
# Version           : 1.0                                                              #
#                                                                                   #
# README dédié au système d'information AirVAM                                         #
#                                                                                   #
# --------------------------------------------------------------------------------- #

├───Sommaire
│   ├───Description du système d'information
│   ├───Architecture et pile logiciel du système d'information
|   ├───Description des variables
|   ├───Flux
|   ├───Architecture et description du livrable
│   └───Synthèse des roles

###### Description du système d'information ######

Rapide descriptif du Système d'Information AirVAM

###### Architecture et pile logiciel du système d'information ######

Descriptif de l'architecture du SI (nombre de serveurs, répartition, rôles).
Détail de la pile logicielle et version utilisée par serveur.

###### Description des variables ######

# all.yml


# app.yml


# database.yml


###### Flux  ######

Pas de flux particulier concernant ce SI

###### Architecture et description du livrable ######

[Lancement manuel du script]

 ansible-playbook site.yml -i inventories/dev/hosts --ask-become-pass --ask-vault-pass

[Architecture_livrable]

├───x.x.x.x
│   ├───livrables
│   │   ├───vx.x.x.x.tar.gz
│   │   └───vx.x.x.x.war
│   ├───scripts
|   |    └───vx.x.x.x.sql
│   ├───documentations
|   |    └───DR_MEX_vX.X.pdf
│   └───projet
|       └───xxx


###### Synthèse des roles ######



##### Infos

Merci à Ludovic TOURNIER (CDAD Toulon) pour le modèle de ce README.
